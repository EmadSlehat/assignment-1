package com.example.emadslehata1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    DatePickerDialog dpDialog;
    EditText etDate;
    Spinner spnBeverageSize;

    String[] provinceList = {"Alberta", "British Columbia", "Manitoba", "New Brunswick", "Newfoundland and Labrador", "Northwest Territories",
            "Nova Scotia", "Nunavut", "Ontario", "Prince Eduard Island", "Saskatchewan", "Quebec", "Yukon" };

    String[] beverageSize = {"Large", "Medium", "Small"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etDate = (EditText)findViewById(R.id.txtDatePicker);
        etDate.setInputType(InputType.TYPE_NULL);

        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cal = Calendar.getInstance();
                int day = cal.get(Calendar.DAY_OF_MONTH);
                int month = cal.get(Calendar.MONTH);
                int year = cal.get(Calendar.YEAR);

                dpDialog = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        etDate.setText(dayOfMonth + "/" + (month+1) + "/" + year);
                    }
                }, day, month, year);

                dpDialog.show();
            }
        });


        AutoCompleteTextView autoProvinces = (AutoCompleteTextView)findViewById(R.id.autoListProvince);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, provinceList);
        autoProvinces.setAdapter(adapter);

        spnBeverageSize = (Spinner) findViewById(R.id.spnBeverageSize);
        ArrayAdapter<String> sizeAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, beverageSize);
        spnBeverageSize.setAdapter(sizeAdapter);

    }
}